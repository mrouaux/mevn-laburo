const express = require('express')
const router = express.Router()

const User = require('../models/User')


// Get all users
router.get('/', async (req, res) => {
    const users = await User.find()
    res.json(users)
})

// Get an especific user
router.get('/:username/:password', async(req, res) => {
    const user = await User.find({username: req.params.username, password: req.params.password})
    console.log(user)
    res.send(user)
})

// Save a user
router.post('/', async (req, res) => {
    const user = new User(req.body)
    await user.save()
    res.json({
        estado: 'User guardado!'
    })
})

/*
// Update a task
router.put('/:_id', async (req, res) => {
    await Task.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        estado: 'Tarea actualizada!'
    })
})
*/
// Delete all users
router.delete('/', async (req, res) => {
    await User.deleteMany()
    res.json({
        estado: "Tarea eliminada!"
    })

})

module.exports = router