const express = require('express')
const app = express()
const morgan = require('morgan')
const mongoose = require('mongoose')

mongoose.connect('mongodb://mongo:27017/mevn-db', {useNewUrlParser: true})
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err))

// Settings
app.set('port', process.env.PORT || 3000)

// Middlewares
app.use(morgan('dev'))
app.use(express.json())

// Routes
    // Tasks
app.use('/tasks',require('./routes/tasks'))
    // Users
app.use('/users', require('./routes/users'))
// Static
app.use(express.static(__dirname + '/public'))

// Start server
app.listen(app.get('port'), () => {
    console.log('Server on port ', app.get('port'))
})