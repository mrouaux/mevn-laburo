import MainMenu from './MainMenu.vue'
import Login from './Login.vue'
import Register from './Register.vue'

export default [
    {path: '/', component: Login},
    {path: '/login', component: Login},
    {path: '/register', component: Register},
    {path: '/index', component: MainMenu},
]